create database warehouseTienda

use warehouseTienda

--Dimension de fecha de calendario (Calendar date dimension)
declare @fecha_inicio as date = '01/01/2015' --El calendario comienza a partir de esta fecha		
declare @fecha_final as date = '1/1/2025' --El calendario termina un dia antes de esta fecha		
create table tiempo (
 id_tiempo int primary key identity(1,1),
 fecha date,
 --En esta dimension encontramos la siguiente jerarquia
 anio smallint,
 mes tinyint,
 dia tinyint,
 dia_enSemana tinyint,
 dia_nombre varchar(10),
 mes_nombre varchar(10),
 dia_enAnio smallint,
 cuatrimestre tinyint,
 mes_fechaInicio date,
 mes_fechaFinal date
)
while @fecha_inicio < @fecha_final
begin
	insert into tiempo(
		fecha, anio, mes, dia, 
		dia_enSemana, dia_nombre, mes_nombre, dia_enAnio, cuatrimestre, 
		mes_fechaInicio, mes_fechaFinal
	)	
	values(
		@fecha_inicio, year(@fecha_inicio), month(@fecha_inicio), day(@fecha_inicio), 
		datepart(weekday, @fecha_inicio), datename(weekday, @fecha_inicio), datename(month, @fecha_inicio), datepart(dayofyear, @fecha_inicio), datepart(quarter, @fecha_inicio),
		dateadd(day,-(day(@fecha_inicio)-1),@fecha_inicio), dateadd(day,-(day(dateadd(month,1,@fecha_inicio))),dateadd(month,1,@fecha_inicio))
	)
	set @fecha_inicio = dateadd(day, 1, @fecha_inicio)
end
--Prueba de dimension tiempo
select top 50 *
from tiempo c
order by newid()




--Dimension "sexo" se refiere al sexo de un individuo
create table sexo (
id_sexo int primary key identity(1,1),
descripcion varchar(20)
)
insert into sexo(descripcion) values 
('Masculino'),
('Femenino'),
('')



--Dimension "Edad" se obtiene al calcular la edad del cliente cuando realizo la compra
--Esta dimension se clasifica como una dimension degenerada
declare @edad_minima int = 18; --Edad minima a partir de la cual es posible expedir factura
declare @edad_maxima int = 99 --Edad maxima, se considera un numero arbitrariamente grande
create table edad(
id_edad int primary key
)
while @edad_minima <= @edad_maxima
begin
	insert into edad(id_edad) values (@edad_minima);
	set @edad_minima = @edad_minima + 1;
end



/*Dimension "Ubicacion" corresponde a la ubicacion geografica, con respecto a la granularidad
es a partir de colonia, siendo colonia la unidad mas peque;a con la que distinguimos diferentes 
zonas geograficas
*/
create table ubicacion(
colonia varchar(50),
cidudad varchar(20),
estado varchar(10),
pais varchar(10)
)
BULK INSERT 
ubicacion
FROM 'C:\Users\uteye\OneDrive\Posgrado\Base de datos 2\shop\coloniasHermosillo.csv'
WITH ( FORMAT='CSV',MAXERRORS = 10000)
alter table ubicacion add id_ubicacion int primary key identity(1,1)
 
--Prueba de bulk insert para colonias de Hermosillo
select top 50 * from ubicacion

--Dimension num_venta corresponde a una dimension basura (junk dimension)
create table num_venta (
num_venta int
)
insert into num_venta(num_venta) values(1)

/* Verificar el limite de sql para referencias entre tablas que no se encuentran en la misma bd
create table TH_ventas(
id_cliente int foreign key(id_cliente) references negocio.dbo.cliente(id_cliente),
id_empleado int foreign key(id_empleado) references negocio.dbo.empleado(id_empleado),
id_sucursal int foreign key(id_sucursal) references negocio.dbo.sucursal(id_sucursal),
id_fecha date foreign key(id_fecha) references dbo.tiempo(id_fecha),
id_ubicacion int foreign key(id_ubicacion) references dbo.ubicacion(id_ubicacion),
id_sexo int foreign key(id_sexo) references dbo.sexo(id_sexo),
id_edad int foreign key references dbo.edad(id_edad),
total_pagar money,
num_ventas int,
id int primary key (id_cliente,id_empleado,id_sucursal,id_fecha,id_ubicacion,id_sexo,id_edad)
)
*/

/* 
Data warehouse para una negocio generico 
*/
create table TH_ventas(
id_cliente int not null , --Estos atributos hacen referencia a la primary key de otra tabla, sin embargo, dado que sql no tiene la capacidad para realizar referencia tipo FK entre bases de datos diferentesid_empleado int not null, --ver comentario en id_cliente
id_empleado int not null,--ver comentario en id_cliente
id_sucursal int not null,--ver comentario en id_cliente
id_tiempo int foreign key(id_tiempo) references dbo.tiempo(id_tiempo) not null,
id_ubicacion int foreign key(id_ubicacion) references dbo.ubicacion(id_ubicacion) not null,
id_sexo int foreign key(id_sexo) references dbo.sexo(id_sexo) not null,
id_edad int foreign key references dbo.edad(id_edad) not null,
total money not null,
num_ventas int not null,
constraint id_TH_ventas primary key clustered (id_cliente,id_empleado,id_sucursal,id_tiempo,id_ubicacion,id_sexo,id_edad) 
)


insert into TH_ventas

 select 
 tienda.dbo.cliente.id_cliente,
 tienda.dbo.empleado.id_empleado,
 tienda.dbo.sucursal.id_sucursal,
 id_tiempo,
 id_ubicacion,
 id_sexo,
 id_edad,
 total,
 num_venta
 from 
 tienda.dbo.cliente,
 tienda.dbo.empleado,
 tienda.dbo.sucursal,
 tiempo,
 ubicacion,
 sexo,
 edad,
 tienda.dbo.ventas,
 num_venta
 where
 tienda.dbo.cliente.id_cliente = tienda.dbo.ventas.id_cliente and
 tienda.dbo.empleado.id_empleado = tienda.dbo.ventas.id_empleado and
 tienda.dbo.sucursal.id_sucursal = tienda.dbo.ventas.id_sucursal and 
 (CONVERT(int,CONVERT(char(8),tienda.dbo.ventas.fecha_venta,112))-CONVERT(char(8),tienda.dbo.cliente.fecha_nacimiento,112))/10000 = edad.id_edad and
 tienda.dbo.ventas.fecha_venta = tiempo.fecha and
 tienda.dbo.cliente.id_direccion = ubicacion.id_ubicacion and
 tienda.dbo.cliente.sexo = sexo.descripcion 
 

select top 100 * from TH_ventas



--Detalle de venta

create table TH_detalle_ventas (
id_sucursal int not null,--ver comentario en id_cliente
id_producto int not null,
id_categoria int not null,
id_subCategoria int not null,
id_tiempo int foreign key(id_tiempo) references dbo.tiempo(id_tiempo) not null,
id_ubicacion int foreign key(id_ubicacion) references dbo.ubicacion(id_ubicacion) not null,
id_sexo int foreign key(id_sexo) references dbo.sexo(id_sexo) not null,
id_edad int foreign key references dbo.edad(id_edad) not null,
monto_producto money not null
constraint id_TH_detalle_ventas primary key clustered (id_sucursal,id_categoria,id_subCategoria,id_producto,id_sexo,id_edad,id_ubicacion,id_tiempo)
)


insert into TH_detalle_ventas
 select
 tienda.dbo.ventas.id_sucursal,
 tienda.dbo.detalle_venta.id_producto,
 tienda.dbo.detalle_venta.id_categoria,
 tienda.dbo.detalle_venta.id_subCategoria,
 id_tiempo,
 id_ubicacion,
 id_sexo,
 id_edad,
 monto_producto
 from 
 tienda.dbo.ventas,
 tienda.dbo.detalle_venta,
 tiempo,
 ubicacion,
 sexo,
 edad,
 tienda.dbo.cliente,
 tienda.dbo.sucursal,
 tienda.dbo.producto,
 tienda.dbo.categoria,
 tienda.dbo.subCategoria
 where
 tienda.dbo.ventas.id_venta = tienda.dbo.detalle_venta.id_venta and
 tienda.dbo.ventas.id_sucursal = tienda.dbo.sucursal.id_sucursal and 
 tienda.dbo.detalle_venta.id_producto = tienda.dbo.producto.id_producto and
 tienda.dbo.detalle_venta.id_categoria = tienda.dbo.categoria.id_categoria and
 tienda.dbo.detalle_venta.id_subCategoria = tienda.dbo.subCategoria.id_subCategoria and 
 tienda.dbo.ventas.fecha_venta = tiempo.fecha and

 tienda.dbo.ventas.id_cliente = tienda.dbo.cliente.id_cliente and
 tienda.dbo.cliente.id_direccion = ubicacion.id_ubicacion and
 tienda.dbo.cliente.sexo = sexo.descripcion and
 (CONVERT(int,CONVERT(char(8),tienda.dbo.ventas.fecha_venta,112))-CONVERT(char(8),tienda.dbo.cliente.fecha_nacimiento,112))/10000 = edad.id_edad
 
 
 select top 50 * from TH_detalle_ventas
 
 

