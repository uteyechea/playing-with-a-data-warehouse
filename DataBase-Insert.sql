--Insert data into db tienda


insert into cliente values 
('Luis','Fernandez','Ramirez','Masculino','RAML19850220ASX','1985/02/20','raml@gmail.com',15),
('Luisa','Tellez','Colosio','Femenino','COTL19870517DRT','1987/05/17','cotl@gmail.com',51),
('Pedro','Jimenez','Fuentes','Masculino','JIFP19780113','1978/01/13','jifp@gmail.com',236),
('Rosa','Peralta','Hurtado','Femenino','PEHR19920505DRT','1992/05/05','pehr@gmail.com',70)
--select top 10 * from cliente

insert into proveedor values
('NewEgg','NEWE19750101EGG','newe@gmail.com',15),
('Amazon','AMZN19900101ZON','amzn@gmail.com',17),
('AliExpress','ALIE20000101ESS','alie@gmail.com',21),
('Ebay','ebay19990101BAY','ebay@gmail.com',40)
select top 5 * from proveedor

insert into sucursal values
('MiTiendita','Juan Cruz',24),
('PartesPc','Silvia Perez',101),
('OfficeMarket','Siria Navarro',299)
select top 3 * from sucursal

insert into puesto values
('Gerente','20250.97'),
('Cajero','10000.00'),
('Vendedor','13000.01')
select top 3 * from puesto

insert into empleado values
--sucursal num. 1
('Irvin','Galaz','Ortiz','GAOI19980518XEA','1998/05/18','gaoi@gmail.com','2017/01/01',1,1,103),
('Jose','de la Fuente','Ontiveros','DEOJ19990101GUT','1999/01/01','deoj@gmail.com','2017/01/01',3,1,110),
('Maria','Perez','Amaya','PEAM20000506ASD','2000/05/06','peam@gmail.com','2017/01/01',2,1,117),
--sucursal num. 2
('Francisca','Jacinto','Cruz','JACF19800122HDR','1980/01/22','jacf@gmail.com','2015/01/01',1,2,201),
('Arnulfo','Castellanos','Olivares','CAOA19810515DGE','1981/05/15','caoa@gmail.com','2015/01/01',2,2,222),
('Jazmin','Puente','Llano','PULJ19821223HTH','1982/12/23','pulj@gmail.com','2015/01/01',3,2,175)
select top 6 * from empleado

insert into categoria values
('Memoria'),
('Disco Duro'),
('Cpu'),
('Monitor'),
('Laptop'),
('pc')
select top 6 * from categoria

insert into subCategoria values
--Categoria "Memoria"
('1000Mhz'), --es mejor hacer los siguiente para encontrar la foreign key: select id_categoria from categoria where categoria='Memoria';
('2000Mhz'),
('3000Mhz'),
--Categoria Disco Duro
('250Gb'),
('500Gb'),
--Categoria Cpu
('2.5Ghz'),
('2.7Ghz'),
--Categoria Monitor
('22in'),
('40in'),
--Categoria Laptop
('Macbook 2020'),
('2020'),
--Categoria PC
('2020'),
('2020')

delete from subCategoria
DBCC CHECKIDENT ('subCategoria', RESEED, 1)
select top 20 * from subCategoria

insert into producto values
('Raptor','',100,1,1,1),
('Raptor','',200,1,2,1),
('Raptor','',300,1,3,1),

('SanDisk','',250,2,4,2),
('SanDisk','',500,2,5,2),
('Intel','',2500,3,6,2),
('AMD','',2700,3,7,2),

('Dell','',1500,4,8,3),
('Gateway','',3000,4,9,3),

('Apple','',20000,5,10,4),
('Dell','',10000,5,11,4),

('Dell','',5000,6,12,4),
('AlienWare','',30000,6,13,4)

select top 20 * from producto

insert into detalle_venta values
((select total from ventas where id_venta=1),1,0.0,(select total from ventas where id_venta=1),1,1,1,1 ),
((select total from ventas where id_venta=2),1,0.0,(select total from ventas where id_venta=2),2,2,1,2 ),
((select total from ventas where id_venta=3),1,0.0,(select total from ventas where id_venta=3),3,3,1,3 ),
((select total from ventas where id_venta=4),1,0.0,(select total from ventas where id_venta=4),4,4,2,4 ),
((select total from ventas where id_venta=5),1,0.0,(select total from ventas where id_venta=5),5,5,2,5 ),
((select total from ventas where id_venta=6),1,0.0,(select total from ventas where id_venta=6),6,6,3,6 ),
((select total from ventas where id_venta=7),1,0.0,(select total from ventas where id_venta=7),7,7,3,7 ),
((select total from ventas where id_venta=8),1,0.0,(select total from ventas where id_venta=8),8,8,4,8 ),
((select total from ventas where id_venta=9),1,0.0,(select total from ventas where id_venta=9),9,9,4,9 ),
((select total from ventas where id_venta=10),1,0.0,(select total from ventas where id_venta=10),10,10,5,10 ),
((select total from ventas where id_venta=11),1,0.0,(select total from ventas where id_venta=9),11,11,5,11 ),
((select total from ventas where id_venta=12),1,0.0,(select total from ventas where id_venta=12),12,12,6,12 ),
((select total from ventas where id_venta=13),1,0.0,(select total from ventas where id_venta=13),13,13,6,13 )

                

--Fix the total
declare @ganancia real = 1.20
insert into ventas values
('2020/01/13',(select precio_deCompra from producto where id_producto=1)*@ganancia, 1,2,3),
('2020/01/13',(select precio_deCompra from producto where id_producto=2)*@ganancia, 2,2,3),
('2020/01/14',(select precio_deCompra from producto where id_producto=3)*@ganancia, 3,1,3),
('2020/01/14',(select precio_deCompra from producto where id_producto=4)*@ganancia, 4,1,3),
('2020/01/14',(select precio_deCompra from producto where id_producto=5)*@ganancia, 1,2,3),
('2020/01/14',(select precio_deCompra from producto where id_producto=6)*@ganancia, 2,2,3),
('2020/01/15',(select precio_deCompra from producto where id_producto=7)*@ganancia, 3,1,3),
('2020/01/15',(select precio_deCompra from producto where id_producto=8)*@ganancia, 4,1,3),
('2020/01/16',(select precio_deCompra from producto where id_producto=9)*@ganancia, 1,2,3),
('2020/01/16',(select precio_deCompra from producto where id_producto=10)*@ganancia, 2,2,3),
('2020/01/16',(select precio_deCompra from producto where id_producto=11)*@ganancia, 3,1,3),
('2020/01/16',(select precio_deCompra from producto where id_producto=12)*@ganancia, 4,1,3),
('2020/01/17',(select precio_deCompra from producto where id_producto=13)*@ganancia, 1,2,3)
select top 20 * from ventas 









select top 20 * from producto












