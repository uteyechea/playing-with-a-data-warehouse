create database tienda

use tienda

/*Catalogo de direcciones en Mexico, note que no incluye la calle, 
incluir todas las calles de un pais no es conveniente, para resolver 
este problema se puede incluir otro atributo para la calle y numero, 
solo en el caso en que dicha informacion sea necesaria.*/
create table direccion(
colonia varchar(40) not null,
municipio varchar(40) not null,
estado varchar(40) not null,
pais varchar(40) not null
)
BULK INSERT 
direccion
FROM 'C:\Users\uteye\OneDrive\Posgrado\Base de datos 2\tarea2\coloniasHermosillo.csv'
WITH ( FORMAT='CSV',MAXERRORS = 10000)
alter table direccion add id_direccion int primary key identity(1,1)
select top 20 * from direccion --Verify bulk insert


--Catalogo de clientes registrados despues de realizar su primer compra
create table cliente(
id_cliente int primary key identity(1,1),
nombre varchar(30) not null,
apellido_paterno varchar(20) not null,
apellido_materno varchar(20) not null,
sexo varchar(20) not null,
rfc varchar(20) not null,
fecha_nacimiento date not null,
correo_electronico varchar(20) not null,
id_direccion int foreign key(id_direccion) references direccion(id_direccion) not null --debe incluir una constriccion de acuerdo al maximo numero de registro en el catalogo "direccion"
)

/*Catalogo de proveedores, puede ser util ya que si se compra mucho 
de algun proveedor en particular es posible negociar mejores precios*/
create table proveedor(
id_proveedor int primary key identity(1,1),
razon_social varchar(20) not null,
rfc varchar(20) not null,
correo_electronico varchar(50) not null,
id_direccion int foreign key(id_direccion) references direccion(id_direccion)
)


--Catalogo de sucursales
create table sucursal(
id_sucursal int primary key identity(1,1),
razon_social varchar(20) not null,
nombre_gerente varchar(20) not null,
id_direccion int foreign key(id_direccion) references direccion(id_direccion)
)

--Catalogo de puestos que pueden ser distribuidos entre los empleados
create table puesto(
id_puesto int primary key identity(1,1),
puesto varchar(20) not null,
sueldo money not null
)

--Catalogo de empleados
create table empleado(
id_empleado int primary key identity(1,1),
nombre varchar(20) not null,
apellido_paterno varchar(20) not null,
apellido_materno varchar(20) not null,
rfc varchar(20) not null,
fecha_nacimiento date not null,
correo_electronico varchar(20) not null,
fecha_contratacion date not null,
id_puesto int foreign key(id_puesto) references puesto(id_puesto) not null,
id_sucursal int foreign key(id_sucursal) references sucursal(id_sucursal) not null,
id_direccion int foreign key(id_direccion) references direccion(id_direccion) not null
)

--Catalogo de categorias de los productos que se compran-venden
create table categoria(
id_categoria int primary key identity(1,1),
categoria varchar(20) not null
)

--Catalogo de Subcategorias de productos que se compran-venden
create table subCategoria(
id_subCategoria int primary key identity(1,1),
subcategoria varchar(20) not null,
)

--Catalogo de productos 
create table producto(
id_producto int primary key identity(1,1),
nombre varchar(20) not null,
descripcion varchar(20) not null,
precio_deCompra money not null,--Permite calcular utlidad, el precio de venta se puede calcular a partir del total que aparece en la tabla venta
id_categoria int foreign key (id_categoria) references categoria(id_categoria) not null,
id_subCategoria int foreign key (id_subCategoria) references subCategoria(id_subCategoria) not null,
id_proveedor int foreign key (id_proveedor) references proveedor(id_proveedor) not null
)

--Tabla transaccional de ventas 
create table ventas (
id_venta int primary key identity(1,1),
fecha_venta date not null,
total money not null,
id_cliente int foreign key (id_cliente) references cliente(id_cliente) not null,
id_sucursal int foreign key (id_sucursal) references sucursal(id_sucursal) not null,
id_empleado int foreign key (id_empleado) references empleado(id_empleado) not null
)

--Tabla transaccional que incluye el detalle de cada venta realizada 
create table detalle_venta(
precio_deVenta money not null,
cantidad int not null,
descuento real not null,
monto_producto money not null,
id_venta int foreign key (id_venta) references ventas(id_venta) not null,
id_producto int foreign key (id_producto) references producto(id_producto) not null,
id_categoria int foreign key(id_categoria) references categoria(id_categoria) not null,
id_subCategoria int foreign key(id_subCategoria) references subCategoria(id_subCategoria) not null,
constraint id_detalle_venta primary key clustered (id_venta,id_producto)
)


